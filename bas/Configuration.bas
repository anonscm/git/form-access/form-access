Attribute VB_Name = "Configuration"
Option Explicit

' Constante utilis�e pour d�finir les droits de l'utilisateur (tous, lecture)
' Concernant tous les droits il faut saisir la valeur de la constante dans le champ "role_ecriture" de la table de configuration
Global Const DROIT_LECTURE As String = "lecture"
Global Const DROIT_ECRITURE As String = "ecriture"
Public Const TOUS As String = "tous"

' Droits par defaut de l'utilisateur connect�. Il est important de positionner les droits par d�faut sur le mode lecture faute de quoi
' tous les utilisateurs disposeront de l'interface en mode modification m�me s'ils ne disposent pas des droits sur les objets de la abse de donn�es.
Global Const DROIT_DEFAUT = DROIT_LECTURE

'Constante permettant de personnaliser la couleur des labels pour les champs dont la saisie est obligatoire. Couleur variable d'Access : vbRed, vbBlack, vbCyan, vbGreen, vbMagenta, vbWhite, vbYellow
Global Const COULEUR_LABEL_OBLIGATOIRE = vbRed

'Constante permettant de personnaliser la couleur des labels pour les champs dont la saisie est facultative. Couleur variable d'Access : vbRed, vbBlack, vbCyan, vbGreen, vbMagenta, vbWhite, vbYellow
Global Const COULEUR_LABEL_OPTIONNEL = vbBlack

Public Const SEPARATEUR_DECIMAL_SYSTEME = ","

'Valeur des identifiants au dela de laquelle on enregistre les nouveaux enregistrements dans la base en mode deconnecte
'Il s'agit ici d'une convention pour travailler avec un champ postgresql de type serial (ou big serial) car en mode deconnecte la base sera une base access
'avec un champ de type entier long qui ne sera pas g�r� automatiquement. Afin d'�viter des conflits lors de la synchronisation �ventuelle avec la base
'centrale postgresql, on renseigne la valeur de ce champ (pour les cr�ations d'enregistrements) avec la premi�re valeur disponible au del� de cette constante;
'le seuil est �lev� et doit repr�senter une valeur qui n'est pas atteinte par le champ de type serial dans la base centrale postgresql (ou autre sgbd)
Public Const ID_MIN_DECONNECTE = 1000000000
